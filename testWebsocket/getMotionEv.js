#!/usr/bin/env node


// Websocket listener
console.log("---- Start repeter on localhost -----");
var W3CWebSocket = require('websocket').w3cwebsocket;
	
var ws = new W3CWebSocket("ws://localhost:2974", "quividi");

ws.onopen=function() {
	ws.send('motion');
}

ws.onclose = function() {
    console.log('echo-protocol Client Closed');
};
 
ws.onmessage = function(e) {
    if (typeof e.data === 'string') {
        console.log("Received: '" + e.data + "'");
    }
};
