import csv

PLAYLOGS = [
    'playlog-2017-01-10.txt',
#    'playlog-2016-06-08.txt',
#    'playlog-2016-06-09.txt',
#    'playlog-2016-06-10.txt',
#    'playlog-2016-06-11.txt',
#    'playlog-2016-06-12.txt',
#    'playlog-2016-06-13.txt',
    ]

total_time = 0
campaigns = {}
#location_id,period_start,gender,age,age_value,glasses,mustache,beard,very_unhappy,unhappy,neutral,happy,very_happy,dwell_time,attention_time
for filename in PLAYLOGS:
    with open(filename, 'r') as f:
        reader = csv.reader(f, delimiter='\t')
        for row in reader:
            # [1]: time as in 2016-06-07 05:34:40
            # [2]: duration
            # [3]: clip ID
            # [5]: campaign ID
            # [7]: screen ID
            dur = int(row[2])
            total_time += dur
            try:
                campaigns[row[5]]['time'] += dur
                campaigns[row[5]]['clips'].add(row[3])
            except KeyError:
                campaigns[row[5]] = {
                    'time': dur,
                    'clips': set(),
                }
                campaigns[row[5]]['clips'].add(row[3])

print 'total ad time: ', total_time, ' sec'
print len(campaigns), ' campaigns'
for k, v in campaigns.iteritems():
    print k, ' : ', len(v['clips']), 'clips, ', v['time'], 'secs (', (100 * v['time']) / total_time, '%)'
