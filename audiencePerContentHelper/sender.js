#!/usr/bin/env node
'use strict';


// Websocket repeiter
var ArgumentParser = require('argparse').ArgumentParser;
var parser = new ArgumentParser({
  version: '0.0.1',
  addHelp:true,
  description: 'Argparse example'
});
parser.addArgument(
  [ '-p', '--clipid' ],
  {
    help: 'the if of your clip that (can be a full name)'
  }
);
parser.addArgument(
  [ '-c', '--campaign' ],
  {
    help: 'the campaign id'
  }
);
parser.addArgument(
  'state',
  {
    help: 'start/stop',
    choices:["start","stop"]
  }
);
var args = parser.parseArgs();




var W3CWebSocket = require('websocket').w3cwebsocket;
	
var ws = new W3CWebSocket("ws://localhost:3974", "clip");
    ws.onopen = function() {
         ws.send(JSON.stringify({"type": args.state,"clipid":args.clipid,"campaignid":args.campaign}));
	 ws.close();
     }
      ws.onmessage = function(evt) {
          console.log("Response:", evt.data);
     }

