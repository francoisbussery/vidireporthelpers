Audience content helper
-----------------------

This tool will help you to trig the audience content api of Vidireport


How to install
--------------


- Install nodejs on your pc
- Install nodejs-websocket
- Install argparse
- Install websocket

npm install nodejs-websocket
npm install argparse
npm install websocket


How to use
----------

in a separate terminal, run the repeater:
nodejs repeter.js 


anywhere, run the helper:

nodejs sender.js --help
usage: sender.js [-h] [-v] [-p CLIPID] [-c CAMPAIGN] {start,stop}

Argparse example

Positional arguments:
  {start,stop}          start/stop

Optional arguments:
  -h, --help            Show this help message and exit.
  -v, --version         Show program's version number and exit.
  -p CLIPID, --clipid CLIPID
                        the if of your clip that (can be a full name)
  -c CAMPAIGN, --campaign CAMPAIGN
                        the campaign id



ex: 

nodejs sender.js  -p "clip nb 1" -c "campaign 1" start
...
nodejs sender.js  -p "clip nb 1" -c "campaign 1" stop

