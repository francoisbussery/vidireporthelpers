#!/usr/bin/env node


// Websocket repeter
console.log("---- Start repeter on localhost -----");
var WebSocketServer = require("nodejs-websocket")
var W3CWebSocket = require('websocket').w3cwebsocket;
	
var ws = new W3CWebSocket("ws://localhost:2974", "quividicontent");


var server = WebSocketServer.createServer({validProtocols: ['clip']},function (conn) {
    console.log("New connection")
    conn.on("text", function (str) {

    console.log("Received "+str)
    ws.send(str);
    })
    conn.on("close", function (code, reason) {
        console.log("Connection closed")
    })
}).listen(3974);
